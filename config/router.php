<?php

use Delight\Router\Router;
use App\Controllers\PagesController;
use App\Controllers\PostsController;
use App\Controllers\AuthorsController;

$router = new Router();


$router->get('/', function (){
    $controller = new PagesController();
    $controller->index();
});

$router->get('/array-and-string', function () {
    $controller = new PagesController();
    $controller->arrayAndString();
});

$router->get('/xml', function() {
    $controller = new PagesController();
    $controller->xmlManipulations();
});

$router->get('/posts', function () {
    $controller = new PostsController();
    $controller->index();
});

$router->get('/post/add', function () {
    $controller = new PostsController();
    $controller->addPost();
});
$router->get('/post/add/:message', function ($message) {
    $controller = new PostsController();
    $controller->addPost($message);
});

$router->post('/post/save', function () {
    $controller = new PostsController();
    $controller->savePost();
});

$router->get('/posts/:from/:to', function ($from, $to) {
    $controller = new PostsController();
    $controller->index(DateTime::createFromFormat('Y-m-d', $from), DateTime::createFromFormat('Y-m-d', $to));
});

$router->get('/posts/:slug', function ($slug){
    $controller = new PostsController();
    $controller->getPost($slug);
});


$router->get('/authors', function () {
    $controller = new AuthorsController();
    $controller->index();
});

$router->get('/authors/:id', function ($id) {
    $controller = new AuthorsController();
    $controller->author($id);
});

$router->get('/authors/:id/posts', function ($id) {
    $controller = new AuthorsController();
    $controller->postsByAuthor($id);
});