<?php
/**
 * Created by PhpStorm.
 * User: steelhammer
 * Date: 2019. 08. 29.
 * Time: 8:49
 */

namespace App;

use App\Helpers\Request;

class Author
{
    public $id;
    public $name;
    public $created_at;
    public $updated_at;

    public function __construct($id, $name, $created_at, $updated_at)
    {
        $this->id = $id;
        $this->name = $name;
        $this->created_at = $created_at;
        $this->updated_at = $updated_at;
    }

}