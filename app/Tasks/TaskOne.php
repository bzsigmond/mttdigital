<?php

namespace App\Tasks;

use App\Helpers\Format;

class TaskOne
{

    public function __construct()
    {

    }

    /**
     * @param array $array
     * @param integer $needle
     * @return array
     */
    public static function findSubArray(array $array, $needle)
    {

        $sum = array_sum($array);

        if ($sum == $needle) {
            return [0, count($array) - 1];
        }

        for ($i = 0; $i < count($array) - 1; $i++) {
            for ($j = count($array) - 1; $j > $i; $j--) {
                $sub = array_slice($array, $i, $j);
                if (array_sum($sub) == $needle) {
                    return [$i, $j];
                }
            }
        }

        return [];
    }


    public static function isPalindrom($string = null)
    {
        $string = Format::trimAllSpace($string);
        $string = Format::removeAccents($string);
        $string = strtolower($string);
        $strLength = strlen($string);
        $strFirstHalf = substr($string, 0, ceil($strLength / 2));
        $strSecondHalf = substr($string, ceil($strLength / 2), $strLength - 1);
        if (strlen($strFirstHalf) > strlen($strSecondHalf)) {
            $strFirstHalf = substr($strFirstHalf, 0, strlen($strFirstHalf) - 1);
        }
        return $strFirstHalf === strrev($strSecondHalf);
    }

    public static function isCorrectBrackets($string)
    {
        $closingBrackets = [
            "(" => ")",
            "[" => "]",
            "{" => "}"
        ];
        $openingBrackets = array_flip($closingBrackets);
        $openings = [];

        $brackets = str_split($string);

        foreach ($brackets as $key => $bracket) {
            if (isset($closingBrackets[$bracket])) {
                $openings[] = $bracket;
            } elseif (isset($openingBrackets[$bracket])) {
                if (array_pop($openings) !== $openingBrackets[$bracket]) {
                    return $key;
                }
            } else {
                throw new \InvalidArgumentException("A megadott string nem csak zárójeleket tartalmaz");
            }
        }

        if (count($openings) == 0) {
            return -1;
        } else {
            return count($brackets) - 1;
        }
    }
}