<?php

namespace App\Tasks;


class TaskTwo
{

    /**
     * @var \DOMDocument;
     */
    private $document;

    public function __construct()
    {
        $this->document = new \DOMDocument();
        $this->document->formatOutput = true;

    }

    public function separateGenre()
    {
        if (!file_exists("data_genres.xml")) {
            $this->document->load("data.xml", LIBXML_NOBLANKS);
            $movies = $this->document->documentElement->getElementsByTagName("movie");

            /**
             * @var $movie \DOMElement
             */
            foreach ($movies as $movie) {

                $genre = $movie->getElementsByTagName("genre")[0];
                $movie->removeChild($movie->getElementsByTagName("genre")[0]);
                $genres = explode("|", $genre->nodeValue);
                $genresDOM = $this->document->createElement("genres");
                foreach ($genres as $genre) {
                    $genresDOM->appendChild($this->document->createElement("genre", $genre));
                }
                $movie->appendChild($genresDOM);
            }
            $this->document->saveXML();
            $this->document->save("data_genres.xml");
        }
    }

    public function separateDate()
    {

        if (!file_exists("data_dates.xml")) {
            $this->document->load("data_genres.xml", LIBXML_NOBLANKS);
            $movies = $this->document->documentElement->getElementsByTagName("movie");
            /**
             * @var $movie \DOMElement
             */
            foreach ($movies as $movie) {
                $title = $movie->getElementsByTagName("title")[0]->nodeValue;
                $date = preg_replace('/[()]/u', '', substr($title, -6));
                $title = substr($title, 0, strlen($title) - 7);

                $movie->getElementsByTagName("title")[0]->nodeValue = htmlspecialchars($title);

                $movie->insertBefore($this->document->createElement("date", $date), $movie->firstChild->nextSibling);

            }
            $this->document->saveXML();
            $this->document->save("data_dates.xml");
        }
    }

    /**
     * @return array|bool
     * @throws \Exception
     */
    public function getHorrorMovies()
    {
        if (!file_exists("data_dates.xml")) {
            return false;
        }
        $this->document->load("data_dates.xml", LIBXML_NOBLANKS);
        $movies = $this->document->documentElement->getElementsByTagName("movie");

        $horrors = [];

        /**
         * @var $movie \DOMElement
         */
        foreach ($movies as $movie) {
            $date = $movie->getElementsByTagName("date")[0]->nodeValue;
            if ($date <= "2010") {
                continue;
            }

            $genres = $movie->getElementsByTagName("genre");
            /**
             * @var $genre \DOMElement
             */
            foreach ($genres as $genre) {
                if ($genre->nodeValue == "Horror") {
                    $horrors[] = [
                        "title" => $movie->getElementsByTagName("title")[0]->nodeValue,
                        "date" => $movie->getElementsByTagName("date")[0]->nodeValue
                    ];
                    break;
                }
            }
        }
        usort($horrors, function ($a, $b) {
           return $a['date'] < $b['date'];
        });
        return $horrors;
    }

}