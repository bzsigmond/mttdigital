<?php
/**
 * Created by PhpStorm.
 * User: steelhammer
 * Date: 2019. 08. 29.
 * Time: 8:59
 */

namespace App\Helpers;


use GuzzleHttp\Client;

class Request
{
    /**
     * @param $type
     * @param $url
     * @param $params
     * @return mixed|\Psr\Http\Message\ResponseInterface
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public static function request($type, $url, $params = [])
    {
        $client = new Client();
        $auth = [
            'headers' => [
                'auth' => "FFA69E043C124403D35887E091EDAE82"
            ]
        ];
        $params = array_merge($params, $auth);
        return $client->request($type, $url, $params);
    }
}