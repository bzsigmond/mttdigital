<?php
/**
 * Created by PhpStorm.
 * User: steelhammer
 * Date: 2019. 08. 29.
 * Time: 8:58
 */

namespace App\Controllers;


use App\Author;
use App\AuthorServiceInterface;
use App\Helpers\Request;

class AuthorsController extends BaseController implements AuthorServiceInterface
{
    /**
     * @return Author[]|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function index()
    {
        $request = Request::request('GET', 'http://interview.mtt.digital:8091/authors');
        $authors = json_decode($request->getBody());
        $this->twig->display('authors.twig', compact('authors'));
    }

    /**
     * @param $id
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function author($id)
    {
        $author = $this->getAuthor($id);
        $author = new Author($author->id, $author->name, $author->created_at, $author->updated_at);
        $this->twig->display('author.twig', compact('author'));
    }

    /**
     * @param $id
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function postsByAuthor($id)
    {
        $author = $this->getAuthor($id);
        $author = new Author($author->id, $author->name, $author->created_at, $author->updated_at);
        $posts = $this->getPostByAuthor($author);
        $this->twig->display('posts.twig', compact('author', 'posts'));
    }

    /**
     * @param $id
     * @return Author|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getAuthor($id)
    {
        $request = Request::request('GET', 'http://interview.mtt.digital:8091/authors/' . $id);
        return json_decode($request->getBody());
    }

    /**
     * @param Author $author
     * @return \App\Post[]|mixed
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function getPostByAuthor(Author $author)
    {
        $request = Request::request('GET', 'http://interview.mtt.digital:8091/authors/' . $author->id . '/posts');
        return json_decode($request->getBody());
    }

}