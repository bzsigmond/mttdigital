<?php
/**
 * Created by PhpStorm.
 * User: steelhammer
 * Date: 2019. 08. 29.
 * Time: 6:53
 */

namespace App\Controllers;

use App\Helpers\Format;
use App\Helpers\Request;
use App\Post;
use App\PostServiceInterface;


class PostsController extends BaseController implements PostServiceInterface
{

    /**
     * @param \DateTime|null $from
     * @param \DateTime|null $to
     * @return Post[]|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function index(\DateTime $from = null, \DateTime $to = null)
    {
        $query = $_GET;
        if ($from) {
            $query['from'] = $from->format('Y-m-d');
        }
        if ($to) {
            $query['to'] = $to->format('Y-m-d');
        }
        $request = Request::request('GET', 'http://interview.mtt.digital:8091/posts', [
            'query' => $query
        ]);
        $posts = json_decode($request->getBody());
        $links = Format::createPagingLinks($posts);
        $date = new \DateTime();
        $lastWeekTo = $lastMonthTo = $date->format('Y-m-d');
        $lastWeekFrom = $date->modify('-1 weeks')->format('Y-m-d');
        $lastMonthFrom = $date->modify('-3 weeks')->format('Y-m-d');
        $this->twig->display('posts.twig', compact('posts', 'links', 'lastWeekTo', 'lastWeekFrom', 'lastMonthTo', 'lastMonthFrom'));
    }

    /**
     * @param null $message
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function addPost($message = null)
    {
        $request = Request::request('GET', 'http://interview.mtt.digital:8091/authors');
        $authors = json_decode($request->getBody());
        $this->twig->display('add-post.twig', compact('authors', 'message'));
    }

    /**
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function savePost() {
        $post = new Post($_POST['name'], $_POST['body'], $_POST['author']);
        $this->insert($post);
        header('Location: /post/add');
    }

    /**
     * @param Post $post
     * @return Post|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     */
    public function insert(Post $post)
    {
        $request = Request::request('POST', 'http://interview.mtt.digital:8091/posts', [
            'body' => json_encode($post),
        ]);
    }

    /**
     * @param $idOrSlug
     * @return Post|void
     * @throws \GuzzleHttp\Exception\GuzzleException
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function getPost($idOrSlug)
    {
        $request = Request::request('GET', 'http://interview.mtt.digital:8091/posts/' . $idOrSlug);
        $post = json_decode($request->getBody());
        $request = Request::request('GET', 'http://interview.mtt.digital:8091/authors/' . $post->author_id);
        $author = json_decode($request->getBody());
        $this->twig->display('post.twig', compact('post', 'author'));
    }

}