<?php
namespace App\Controllers;

class BaseController
{

    protected $twig;

    public function __construct()
    {
        $loader = new \Twig\Loader\FilesystemLoader(  'views');
        $this->twig = new \Twig\Environment($loader, [
            'cache' => 'cache/views',
            'auto_reload' => true, // disable cache
        ]);

    }
}