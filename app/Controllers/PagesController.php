<?php

namespace App\Controllers;

use App\Tasks\TaskOne;
use App\Helpers\Format;
use App\Tasks\TaskTwo;

class PagesController extends BaseController
{
    /**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function index()
    {
        $this->twig->display("index.twig");
    }

    /**
     * @throws \Twig\Error\LoaderError
     * @throws \Twig\Error\RuntimeError
     * @throws \Twig\Error\SyntaxError
     */
    public function arrayAndString()
    {
        $taskSubArray = Format::arrayToString(TaskOne::findSubArray([2, 7, 6, 2, 1, 1, 8], 12));
        $testSubArray = Format::arrayToString(TaskOne::findSubArray([3, 4, 5, 1, 2, 6, 7], 10));

        $testPalindromOne = TaskOne::isPalindrom("A bihari hír a hiba");
        $testPalindromTwo = TaskOne::isPalindrom("Nem Palindrom");

        $testBracketsOne = TaskOne::isCorrectBrackets("()[{}()]");
        $testBracketsTwo = TaskOne::isCorrectBrackets("([{]})");

        $this->twig->display("task-one.twig", compact(
            'taskSubArray',
            'testSubArray',
            'testPalindromOne',
            'testPalindromTwo',
            'testBracketsOne',
            'testBracketsTwo'
        ));
    }

    /**
     * @throws \Exception
     */
    public function xmlManipulations()
    {
        $task = new TaskTwo();
        $task->separateGenre();
        $task->separateDate();
        $movies = $task->getHorrorMovies();
        $this->twig->display('task-two.twig', compact('movies'));
    }
}