<?php

namespace App;

interface PostServiceInterface
{
    /**
     * @return Post[]
     */
    function index(\DateTime $from = null, \DateTime $to = null);

    /**
     * @return Post
     */
    function getPost($idOrSlug);

    /**
     * @return Post
     */
    function insert(Post $post);
}
