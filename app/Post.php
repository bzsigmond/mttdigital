<?php
/**
 * Created by PhpStorm.
 * User: steelhammer
 * Date: 2019. 08. 29.
 * Time: 6:44
 */

namespace App;


use App\Helpers\Request;

class Post
{
    public $name;

    public $body;

    public $author_id;

    public function __construct($name, $body, $author_id)
    {
        $this->name = $name;
        $this->body = $body;
        $this->author_id = $author_id;
    }
}